"use strict";


function startTime() {
    let today = new Date();
    let hr = today.getHours();
    let min = today.getMinutes();
    let sec = today.getSeconds();
   
    min = checkTime(min);
    sec = checkTime(sec);
    document.body.innerHTML = hr + " : " + min + " : " + sec;
    let time = setTimeout(function(){ startTime() }, 1000);
}
function checkTime(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}

startTime();